﻿/* File Name : LoadBalancer.cs
 * Author : Tristyn Mackay
 * Date Created : 21/09/2017
 * Date Edited : 25/09/2017
 * 
 * Description : A Load Balancer used to distribute requests to multiple backends/servers to split a work load.
 * 
 * Copyright : Tristyn Mackay 2017.
 */

using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace LoadBalancerLibrary
{
    public class LoadBalancer
    {
        #region - Variables.
        // Amount of queues.
        private int queueCount;

        // Queues.
        private List<Queue<Request>> requests;
        private List<Queue<Response>> responses;

        // Queue timers.
        private float[] queueTimers;

        // Connection information.
        private bool connectionsAreDefault;
        private String defaultConnection;
        private List<String> connections;

        // Time information.
        private DateTime startTime;

        // Log information.
        private static Logger logger;
        #endregion


        #region - Constructors.
        /// <summary>
        /// Default Constructor. Creates one (1) set of queues and uses a default connection.
        /// </summary>
        public LoadBalancer(String defaultConnectionString)
        {
            if (String.IsNullOrEmpty(defaultConnectionString))
            {
                String errorMessage = "Couldn't create the Load Balancer as no default connection string was given.\n" +
                                        "    Please provide a connection string to use as a default.";
                logger.logError(errorMessage, "Default Connection String Empty", "Constructor Error");
                throw new Exception(errorMessage);
            }
            else
            {
                // Set the count for the amount of queues. 
                queueCount = 1;

                // Setup the requests and responses Queues.
                requests = new List<Queue<Request>>();
                responses = new List<Queue<Response>>();
                for (int i = 0; i < queueCount; i++)
                {
                    requests.Add(new Queue<Request>());
                    responses.Add(new Queue<Response>());
                }

                // Setup the timers for each queue to run.
                queueTimers = new float[queueCount];
                queueTimers[0] = 1.0f;

                // Setup connections details.
                connectionsAreDefault = true;
                defaultConnection = defaultConnectionString;

                // Setup the logger.
                startLoadBalancer();
            }
        }

        /// <summary>
        /// Overloaded Constructor. Creates multiple queues for distribution of load.
        /// Sets a default connection corresponding to each queue.
        /// </summary>
        /// <param name="queueTimers"></param>
        /// <param name="defaultConnectionString"></param>
        public LoadBalancer(float[] queueTimers, String defaultConnectionString)
        {
            if (queueTimers == null || queueTimers.Count() == 0)
            {
                String errorMessage = "Couldn't create the Load Balancer as there were no queues to be created.\n" +
                                        "    Please fill in queueTimers float[] correctly.";
                logger.logError(errorMessage, "No Queue Timers Supplied", "Constructor Error");
                throw new Exception(errorMessage);
            }
            else if (String.IsNullOrEmpty(defaultConnectionString))
            {
                String errorMessage = "Couldn't create the Load Balancer as no default connection string was given.\n" +
                                        "    Please provide a connection string to use as a default.";
                logger.logError(errorMessage, "Default Connection String Empty", "Constructor Error");
                throw new Exception(errorMessage);
            }
            else
            {
                // Set the count for the amount of queues. 
                queueCount = queueTimers.Count();

                // Setup the requests and responses Queues.
                requests = new List<Queue<Request>>();
                responses = new List<Queue<Response>>();
                for (int i = 0; i < queueCount; i++)
                {
                    requests.Add(new Queue<Request>());
                    responses.Add(new Queue<Response>());
                }

                // Setup the timers for each queue to run.
                this.queueTimers = queueTimers;

                // Setup connections details.
                connectionsAreDefault = true;
                defaultConnection = defaultConnectionString;

                // Setup the logger.
                startLoadBalancer();
            }
        }

        /// <summary>
        /// Overloaded Constructor. Creates multiple queues for distribution of load.
        /// Sets multiple connections each corresponding to a specific queue.
        /// There must be an equal amount of connection strings as there are queue timers/Queues,
        /// i.e 3 response queues = 3 queue timers = 3 connection strings.
        /// </summary>
        /// <param name="queueTimers"></param>
        /// <param name="connectionStrings"></param>
        public LoadBalancer(float[] queueTimers, String[] connectionStrings)
        {
            if (queueTimers == null || queueTimers.Count() == 0)
            {
                String errorMessage = "Couldn't create the Load Balancer as there were no queues to be created.\n" +
                                        "    Please fill in queueTimers float[] correctly.";
                logger.logError(errorMessage, "No Queue Timers Supplied", "Constructor Error");
                throw new Exception(errorMessage);
            }
            else if (connectionStrings == null || connectionStrings.Count() == 0)
            {
                String errorMessage = "Couldn't create the Load Balancer as there were not enough connection strings to setup for queues.\n" +
                                        "    Please fill in connection strings String[] correctly.";
                logger.logError(errorMessage, "Not Enough Connection Strings", "Constructor Error");
                throw new Exception(errorMessage);
            }
            else if (queueTimers.Count() != connectionStrings.Count())
            {
                String errorMessage = "Couldn't create the Load Balancer due to differing amounts of queues to connection strings.\n" +
                                        "    There must be an equal amount of connection strings to queues.";
                logger.logError(errorMessage, "Uneven Amount of Queues or Connection Strings", "Constructor Error");
                throw new Exception(errorMessage);
            }
            else
            {
                // Set the count for the amount of queues. 
                queueCount = queueTimers.Count();

                // Setup the requests and responses Queues.
                requests = new List<Queue<Request>>();
                responses = new List<Queue<Response>>();
                for (int i = 0; i < queueCount; i++)
                {
                    requests.Add(new Queue<Request>());
                    responses.Add(new Queue<Response>());
                }

                // Setup the timers for each queue to run.
                this.queueTimers = queueTimers;

                // Setup connections details.
                connectionsAreDefault = false;
                connections = new List<String>();
                foreach (String connection in connectionStrings)
                {
                    connections.Add(connection);
                }
                
                // Setup the logger.
                startLoadBalancer();
            }
        }

        /// <summary>
        /// Copy Constructor.
        /// </summary>
        /// <param name="loadBalancer"></param>
        public LoadBalancer(LoadBalancer loadBalancer)
        {
            if (loadBalancer == null)
            {
                String errorMessage = "Couldn't copy the load balancer as the load balancer provided does not exist.\n" +
                                        "    Please provide a valid instance of a load balancer.";
                logger.logError(errorMessage, "Incorrect Load Balancer Provided", "Constructor Error");
                throw new Exception(errorMessage);
            }
            else
            {
                // Copy the data from the given load balancer to this one.
                queueCount = loadBalancer.queueCount;

                requests = loadBalancer.requests;
                responses = loadBalancer.responses;

                queueTimers = loadBalancer.queueTimers;

                connectionsAreDefault = loadBalancer.connectionsAreDefault;
                defaultConnection = loadBalancer.defaultConnection;
                connections = loadBalancer.connections;
            }
            
            // Setup the logger.
            startLoadBalancer();
        }
        #endregion


        #region - Accessors.
        public int getQueueCount()
        {
            return queueCount;
        }

        public DateTime getStartTime()
        {
            return startTime;
        }
        #endregion


        //TODO
        #region - Methods.
        //TODO
        #region - Request Methods.
        /// <summary>
        /// Add a new request to the specified requests Queue.
        /// </summary>
        /// <param name="newRequest"></param>
        /// <param name="queueNumber"></param>
        public void addRequest(Request newRequest, int queueNumber)
        {
            if (queueNumber < 0 || queueNumber >= requests.Count())
            {
                String errorMessage = "Couldn't add the request as the number of the queue to add to does not exist.";
                logger.logError(errorMessage, "Index Out of Bounds or NULL QUEUE", "Request Error");
                throw new Exception(errorMessage);
            }
            else if (newRequest == null)
            {
                String errorMessage = "Couldn't add the request as the request was null.";
                logger.logError(errorMessage, "NULL Request", "Request Error");
                throw new Exception(errorMessage);
            }
            else
            {
                requests[queueNumber].Enqueue(newRequest);
                logger.addRequest(newRequest);
            }
        }

        //TODO
        /// <summary>
        /// Complete and remove a request from the specified requests Queue.
        /// </summary>
        /// <param name="queueNumber"></param>
        /// <returns></returns>
        public Response completeRequest(int queueNumber)
        {
            Request request = requests[queueNumber].Dequeue();

            // Perform server connection via corresponding connection string identified by queueNumber or default connection string.
            //TODO

            // Perform request.
            //TODO

            // Log the completed request.
            logger.completeRequest(request);

            // Create and return the response
            return addResponse(request, new DataTable()/* table of data from the DB*/, queueNumber);
        }
        #endregion


        //TODO
        #region - Response Methods.
        /// <summary>
        /// Add a new response to the specified responses Queue.
        /// </summary>
        /// <param name="request"></param>
        /// <param name="table"></param>
        /// <param name="queueNumber"></param>
        /// <returns></returns>
        private Response addResponse(Request request, DataTable table, int queueNumber)
        {
            // Create a Response from a request and a data table returned from the database.
            Response response = new Response(request.getOwner(), request.getCommand(), request.getCommandType(), table);

            // Add the response to the corresponding responses queue.
            responses[queueNumber].Enqueue(response);

            // Log the response creation.
            logger.addResponse(response);

            return response;
        }

        //TODO
        /// <summary>
        /// Complete and remove a response from the responses Queue.
        /// </summary>
        /// <param name="queueNumber"></param>
        public void completeResponse(int queueNumber)
        {
            // Dequeue a response from the corresponding response queue.
            Response response = responses[queueNumber].Dequeue();

            // Send the response back to the user.
            //TODO

            // Log the response completion;
            logger.completeResponse(response);
        }
        #endregion


        #region - Generic Load Balancer Methods.
        /// <summary>
        /// Used for start up of the load balancer to finish initialisation.
        /// </summary>
        private void startLoadBalancer()
        {
            // Set the event to be fired before closing the application.
            AppDomain.CurrentDomain.ProcessExit += new EventHandler(OnProcessExit);

            // Set the start time of the load balancer to now.
            startTime = DateTime.Now;

            // Create the logger and initialise it.
            logger = new Logger(this);
        }


        /// <summary>
        /// Finalise any last steps before shutting down the load balancer.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        static void OnProcessExit(object sender, EventArgs e)
        {
            // Perform the final log entry before closing.
            logger.stopLogs();
        }
        #endregion
        #endregion
    }
}

﻿/* File Name : Logger.cs
 * Author : Tristyn Mackay
 * Date Created : 22/09/2017
 * Date Edited : 25/09/2017
 * 
 * Description : A Logger class used for logging events to text files in the project's Logs folder.
 * 
 * Copyright : Tristyn Mackay 2017.
 */

using System;
using System.IO;

namespace LoadBalancerLibrary
{
    class Logger
    {
        #region - Variables.
        // Log file paths.
        private string masterLogPath;
        private string runLogPath;
        private string requestLogPath;
        private string responseLogPath;
        private string errorLogPath;
        // Log file streams.
        private FileStream masterLog;
        private FileStream runLog;
        private FileStream requestLog;
        private FileStream responseLog;
        private FileStream errorLog;
        #endregion


        #region - Constructors.
        /// <summary>
        /// Default Constructor. Requires a Load Balancer to properly initialise.
        /// </summary>
        /// <param name="loadBalancer"></param>
        public Logger(LoadBalancer loadBalancer)
        {
            startLogs(loadBalancer);
        }
        #endregion


        #region - Methods.
        #region - Generic Log Methods.
        public void startLogs(LoadBalancer loadBalancer)
        {
            String logDirectoryPath = Directory.GetCurrentDirectory() + "/Logs";

            #region - Set the path names for all the different logs.
            masterLogPath = logDirectoryPath + "/MasterLog.txt";
            runLogPath = logDirectoryPath + "/RunLog.txt";
            requestLogPath = logDirectoryPath + "/RequestLog.txt";
            responseLogPath = logDirectoryPath + "/ResponseLog.txt";
            errorLogPath = logDirectoryPath + "/ErrorLog.txt";
            #endregion

            // Check if the logs folder exists.
            if (!Directory.Exists(logDirectoryPath))
            {
                Directory.CreateDirectory(logDirectoryPath);
            }

            #region - Check if log files exist.
            // Check if the master log file exists.
            if (!File.Exists(masterLogPath))
            {
                File.Create(masterLogPath);
            }
            // Check if the run log file exists.
            if (!File.Exists(runLogPath))
            {
                File.Create(runLogPath);
            }
            // Check if the request log file exists.
            if (!File.Exists(requestLogPath))
            {
                File.Create(requestLogPath);
            }
            // Check if the response log file exists.
            if (!File.Exists(responseLogPath))
            {
                File.Create(responseLogPath);
            }
            // Check if the error log file exists.
            if (!File.Exists(errorLogPath))
            {
                File.Create(errorLogPath);
            }
            #endregion

            // Setup the initial start string for each log.
            String logStartString = "\n********************************************************************************" +
                                    "\n--Start Time : " + loadBalancer.getStartTime() + ".--\n";

            #region - Write the start string to each of the logs.
            // Write the start string to the master log.
            using (masterLog = new FileStream(masterLogPath, FileMode.Append, FileAccess.Write))
            {
                using (StreamWriter sw = new StreamWriter(masterLog))
                {
                    sw.WriteLine(logStartString + "--    Amount of Queues Used = " + loadBalancer.getQueueCount() + "--\n");
                }
            }
            // Write the start string to the run log.
            using (runLog = new FileStream(runLogPath, FileMode.Append, FileAccess.Write))
            {
                using (StreamWriter sw = new StreamWriter(runLog))
                {
                    sw.WriteLine(logStartString + "--    Amount of Queues Used = " + loadBalancer.getQueueCount() + "--\n");
                }
            }
            // Write the start string to the request log.
            using (requestLog = new FileStream(requestLogPath, FileMode.Append, FileAccess.Write))
            {
                using (StreamWriter sw = new StreamWriter(requestLog))
                {
                    sw.WriteLine(logStartString);
                }
            }
            // Write the start string to the response log.
            using (responseLog = new FileStream(responseLogPath, FileMode.Append, FileAccess.Write))
            {
                using (StreamWriter sw = new StreamWriter(responseLog))
                {
                    sw.WriteLine(logStartString);
                }
            }
            // Write the start string to the error log.
            using (errorLog = new FileStream(errorLogPath, FileMode.Append, FileAccess.Write))
            {
                using (StreamWriter sw = new StreamWriter(errorLog))
                {
                    sw.WriteLine(logStartString);
                }
            }
            #endregion
        }

        public void stopLogs()
        {
            DateTime endTime = DateTime.Now;

            // Setup the end string for each log.
            String logEndString = "\n--End Time : " + endTime + ".--\n" +
                                    "********************************************************************************";

            #region - Write the end string to each of the logs.
            // Write the end string to the master log.
            using (masterLog = new FileStream(masterLogPath, FileMode.Append, FileAccess.Write))
            {
                using (StreamWriter sw = new StreamWriter(masterLog))
                {
                    sw.WriteLine(logEndString);
                }
            }
            // Write the end string to the run log.
            using (runLog = new FileStream(runLogPath, FileMode.Append, FileAccess.Write))
            {
                using (StreamWriter sw = new StreamWriter(runLog))
                {
                    sw.WriteLine(logEndString);
                }
            }
            // Write the end string to the request log.
            using (requestLog = new FileStream(requestLogPath, FileMode.Append, FileAccess.Write))
            {
                using (StreamWriter sw = new StreamWriter(requestLog))
                {
                    sw.WriteLine(logEndString);
                }
            }
            // Write the end string to the response log.
            using (responseLog = new FileStream(responseLogPath, FileMode.Append, FileAccess.Write))
            {
                using (StreamWriter sw = new StreamWriter(responseLog))
                {
                    sw.WriteLine(logEndString);
                }
            }
            // Write the end string to the error log.
            using (errorLog = new FileStream(errorLogPath, FileMode.Append, FileAccess.Write))
            {
                using (StreamWriter sw = new StreamWriter(errorLog))
                {
                    sw.WriteLine(logEndString);
                }
            }
            #endregion
        }
        #endregion


        #region - Request Log Methods.
        /// <summary>
        /// Write the request to the corresponding log files.
        /// </summary>
        /// <param name="requestString"></param>
        private void writeRequest(String requestString)
        {
            #region - Write the string representation of the request to each of the logs.
            // Write the request string to the master log.
            using (masterLog = new FileStream(masterLogPath, FileMode.Append, FileAccess.Write))
            {
                using (StreamWriter sw = new StreamWriter(masterLog))
                {
                    sw.WriteLine(requestString);
                }
            }
            // Write the request string to the request log.
            using (requestLog = new FileStream(requestLogPath, FileMode.Append, FileAccess.Write))
            {
                using (StreamWriter sw = new StreamWriter(requestLog))
                {
                    sw.WriteLine(requestString);
                }
            }
            #endregion
        }

        /// <summary>
        /// Generate a string representation of the request that was added, then write it to the logs.
        /// </summary>
        /// <param name="request"></param>
        public void addRequest(Request request)
        {
            String requestString = "[" + request.getStartTime() + "] *REQUEST CREATED* -\n" +
                                    "    Owner = '" + request.getOwner() + "',\n" +
                                    "    Command Type = '" + request.getCommandType().ToString() + "',\n" +
                                    "    Command = '" + request.getCommand() + "';\n";

            writeRequest(requestString);
        }

        /// <summary>
        /// Generate a string representation of the request that was completed, then write it to the logs.
        /// </summary>
        /// <param name="request"></param>
        public void completeRequest(Request request)
        {
            DateTime now = DateTime.Now;
            String requestString = "[" + now + "] *REQUEST COMPLETED* -\n" +
                                    "    Owner = '" + request.getOwner() + "',\n" +
                                    "    Command Type = '" + request.getCommandType().ToString() + "',\n" +
                                    "    Command = '" + request.getCommand() + "',\n" +
                                    "    Time Taken = " + getTimeDifference(now, request.getStartTime()) + ";\n";

            writeRequest(requestString);
        }
        #endregion


        #region - Response Log Methods.
        /// <summary>
        /// Write the response to the corresponding log files.
        /// </summary>
        /// <param name="responseString"></param>
        private void writeResponse(String responseString)
        {
            #region - Write the string representation of the response to each of the logs.
            // Write the request string to the master log.
            using (masterLog = new FileStream(masterLogPath, FileMode.Append, FileAccess.Write))
            {
                using (StreamWriter sw = new StreamWriter(masterLog))
                {
                    sw.WriteLine(responseString);
                }
            }
            // Write the request string to the request log.
            using (responseLog = new FileStream(responseLogPath, FileMode.Append, FileAccess.Write))
            {
                using (StreamWriter sw = new StreamWriter(requestLog))
                {
                    sw.WriteLine(responseString);
                }
            }
            #endregion
        }

        /// <summary>
        /// Generate a string representation of the response that was added, then write it to the logs.
        /// </summary>
        /// <param name="response"></param>
        public void addResponse(Response response)
        {
            String responseString = "[" + response.getStartTime() + "] *RESPONSE CREATED* -\n" +
                                    "    Owner = '" + response.getOwner() + "',\n" +
                                    "    Command Type = '" + response.getCommandType().ToString() + "',\n" +
                                    "    Command = '" + response.getCommand() + "';\n";

            writeResponse(responseString);
        }

        /// <summary>
        /// Generate a string representation of the response that was completed, then write it to the logs.
        /// </summary>
        /// <param name="response"></param>
        public void completeResponse(Response response)
        {
            DateTime now = DateTime.Now;
            String responseString = "[" + now + "] *RESPONSE COMPLETED* -\n" +
                                    "    Owner = '" + response.getOwner() + "',\n" +
                                    "    Command Type = '" + response.getCommandType().ToString() + "',\n" +
                                    "    Command = '" + response.getCommand() + "',\n" +
                                    "    Time Taken = " + getTimeDifference(now, response.getStartTime()) + ";\n";

            writeResponse(responseString);
        }
        #endregion


        #region - Error Log Methods.
        /// <summary>
        /// Write the error to the corresponding log files.
        /// </summary>
        /// <param name="requestString"></param>
        public void logError(String error, String errorName, String errorType)
        {
            String errorString = "[" + DateTime.Now + "] *ERROR* -\n" +
                                    "    Error Type = '" + errorType + "',\n" +
                                    "    Error Name = '" + errorName + "',\n" +
                                    "    Error Message = '" + error + "';\n";

            #region - Write the string representation of the error to each of the logs.
            // Write the request string to the master log.
            using (masterLog = new FileStream(masterLogPath, FileMode.Append, FileAccess.Write))
            {
                using (StreamWriter sw = new StreamWriter(masterLog))
                {
                    sw.WriteLine(errorString);
                }
            }
            // Write the request string to the request log.
            using (errorLog = new FileStream(errorLogPath, FileMode.Append, FileAccess.Write))
            {
                using (StreamWriter sw = new StreamWriter(errorLog))
                {
                    sw.WriteLine(errorString);
                }
            }
            #endregion
        }
        #endregion


        #region - Time Comparisson Methods.
        private String getTimeDifference(DateTime now, DateTime startTime)
        {
            return (now - startTime).ToString(@"hh\:mm\:ss");
        }
        #endregion
        #endregion
    }
}

﻿/* File Name : Request.cs
 * Author : Tristyn Mackay
 * Date Created : 21/09/2017
 * Date Edited : 25/09/2017
 * 
 * Description : A request used to hold information about an owner and a command to be completed.
 * 
 * Copyright : Tristyn Mackay 2017.
 */

using System;

namespace LoadBalancerLibrary
{
    public class Request
    {
        #region - Variables.
        private String owner;

        private DateTime startTime;

        private String command;

        private CommandType commandType;

        public enum CommandType
        {
            SELECT,
            INSERT,
            UPDATE,
            DELETE
        }
        #endregion


        #region - Constructors.
        /// <summary>
        /// Default constructor.
        /// </summary>
        /// <param name="owner"></param>
        /// <param name="command"></param>
        /// <param name="commandType"></param>
        public Request(String owner, String command, CommandType commandType)
        {
            startTime = DateTime.Now;
            this.owner = owner;
            this.command = command;
            this.commandType = commandType;
        }
        #endregion


        #region - Accessors.
        public String getOwner()
        {
            return owner;
        }

        public DateTime getStartTime()
        {
            return startTime;
        }

        public String getCommand()
        {
            return command;
        }

        public CommandType getCommandType()
        {
            return commandType;
        }
        #endregion
    }
}

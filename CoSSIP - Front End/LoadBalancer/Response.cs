﻿/* File Name : Response.cs
 * Author : Tristyn Mackay
 * Date Created : 21/09/2017
 * Date Edited : 25/09/2017
 * 
 * Description : A response used to hold information about an owner, a command that has been completed,
 *               and the resulting data.
 * 
 * Copyright : Tristyn Mackay 2017.
 */

using System;
using System.Data;

namespace LoadBalancerLibrary
{
    public class Response
    {
        #region - Variables.
        private String owner;

        private DateTime startTime;

        private String command;
        
        private Request.CommandType commandType;

        private DataTable table;
        #endregion


        #region - Constructors.
        /// <summary>
        /// Default Constructor.
        /// </summary>
        /// <param name="owner"></param>
        /// <param name="command"></param>
        /// <param name="commandType"></param>
        /// <param name="table"></param>
        public Response(String owner, String command, Request.CommandType commandType, DataTable table)
        {
            startTime = DateTime.Now;
            this.owner = owner;
            this.command = command;
            this.commandType = commandType;
            this.table = table;
        }
        #endregion


        #region - Accessors.
        public String getOwner()
        {
            return owner;
        }
        
        public DateTime getStartTime()
        {
            return startTime;
        }

        public String getCommand()
        {
            return command;
        }

        public Request.CommandType getCommandType()
        {
            return commandType;
        }

        public DataTable getTable()
        {
            return table;
        }
        #endregion
    }
}

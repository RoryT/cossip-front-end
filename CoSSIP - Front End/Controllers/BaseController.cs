﻿using System.Web.Mvc;
using CoSSIP___Front_End.Models;

namespace CoSSIP___Front_End.Controllers
{
    public class BaseController : Controller
    {
        protected override void OnActionExecuted(ActionExecutedContext filterContext)
        {
            base.OnActionExecuted(filterContext);

            ViewModelBase model = filterContext.Controller.ViewData.Model as ViewModelBase;
        }
    }
}
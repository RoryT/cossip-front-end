﻿/*
 * File Name - CoSSIPController.cs
 *
 * Author - Rory Thoman
 * Date Created - 06/10/2017
 * Last Edited - 26/10/2017
 * Last Edited by - Tristyn Mackay
 *
 * Description -
 */

using System.Web.Mvc;
using System.Web.Configuration;
using CoSSIP___Front_End.Models;
using Newtonsoft.Json;
using System.Web.Script.Serialization;

namespace CoSSIP___Front_End.Controllers
{

    public class CoSSIPController : Controller
    {
        private SIP_Repository db = new SIP_Repository(WebConfigurationManager.ConnectionStrings["dbConnectionString"].ConnectionString);

        private string username = null;



        /// <summary>
        /// This is the generic model that will be returned on all pages.
        /// </summary>
        /// <returns></returns>
        public ActionResult getModel()
        {
            ViewData.Model = new ViewModelBase { user = db.GetUser(userName()) };
            return View();
        }

        #region - Create Views(Pages) Methods.
        /// <summary>
        /// Get the website's 'Home' page.
        /// </summary>
        /// <returns></returns>
        public ActionResult Home()
        {
            return getModel();
        }

        /// <summary>
        /// Get the website's 'Template' page.
        /// </summary>
        /// <returns></returns>
        public ActionResult Template()
        {
            if (checkIfGeneralAccess()) {
                ViewData.Model = new TemplatePageModel { user = db.GetUser(userName()), Tables = db.GetAllTables(), Fields = db.GetAllFields(), FieldTypes = db.GetAllFilters() };
                return View();
            }
            return RedirectToAction("Home");
        }
        
        /// <summary>
        /// Get the website's 'About' page.
        /// </summary>
        /// <returns></returns>
        public ActionResult About()
        {
            return getModel();
        }

        /// <summary>
        /// Get the website's 'Contact' page.
        /// </summary>
        /// <returns></returns>
        public ActionResult Contact()
        {
            return getModel();
        }

        /// <summary>
        /// Get the website's 'Help' page.
        /// </summary>
        /// <returns></returns>
        public ActionResult Help()
        {
            return getModel();
        }

        /// <summary>
        /// Get the website's 'Documentation' page.
        /// </summary>
        /// <returns></returns>
        public ActionResult Documentation()
        {
            return getModel();
        }

        /// <summary>
        /// Get the website's 'Options' page.
        /// </summary>
        /// <returns></returns>
        public ActionResult Options()
        {
            if (checkIfAdminAccess()) {
                return getModel();
            }
            return View();
        }
        #endregion


        #region - User related methods.
        /// <summary>
        /// Get a user's user name for SIP from their AD Account.
        /// </summary>
        /// <param name="nameWithDom"></param>
        /// <returns></returns>
        public string userName(string nameWithDom)
        {
            string[] splitName;

            splitName = nameWithDom.Split('\\');

            if (splitName.Length > 0)
            {
                return splitName[splitName.Length - 1];
            }
            else
            {
                return splitName[0];
            }
        }

        /// <summary>
        /// Get the SIP user name.
        /// </summary>
        /// <returns></returns>
        public string userName()
        {
            if (username == null)
            {
                string[] splitName;

                splitName = System.Web.HttpContext.Current.User.Identity.Name.Split('\\');

                if (splitName.Length > 0)
                {
                    return splitName[splitName.Length - 1];
                }
                else
                {
                    return splitName[0];
                }
            }
            else
            {
                return username;
            }
        }

        /// <summary>
        /// Use this to check if the current user has 'general' access level.
        /// </summary>
        /// <returns></returns>
        public bool checkIfGeneralAccess()
        {
            string[] userRoles = db.GetUserRoles(userName());

            foreach (string userRole in userRoles)
            {
                if (userRole.Equals("General") || userRole.Equals("Admin"))
                {
                    return true;
                }
            }
            return false;
        }

        /// <summary>
        /// Use this to check if the current user has 'admin' access level.
        /// </summary>
        /// <returns></returns>
        public bool checkIfAdminAccess()
        {
            string[] userRoles = db.GetUserRoles(userName());

            foreach (string userRole in userRoles)
            {
                if (userRole.Equals("Admin"))
                {
                    return true;
                }
            }
            return false;
        }
        #endregion


        #region - POST Create Methods.
        /// <summary>
        /// Add a user to the database.
        /// </summary>
        /// <param name="userLogin"></param>
        /// <param name="userFirstName"></param>
        /// <param name="userLastName"></param>
        /// <param name="userRole"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult AddUser(string userLogin, string userFirstName, string userLastName, string userRole)
        {
            if (checkIfAdminAccess())
            {
                db.addUser(userLogin, userFirstName, userLastName, userRole);
            }
            return View();
        }

        /// <summary>
        /// Add an access Role to the database.
        /// </summary>
        /// <param name="roleName"></param>
        /// <param name="roleDescription"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult AddRole(string roleName, string roleDescription)
        {
            if (checkIfAdminAccess())
            {
                db.addRole(roleName, roleDescription);
            }
            return View();
        }
        #endregion

        #region - POST View Methods.
        [HttpPost]
        public ActionResult PostTemplateSave(TemplateModel template)
        {
            db.addTemplate(userName(), template.templateName, template.markup, template.tables, template.fields, template.filters, template.layoutMarkup);

            return Json(new { success = true });
        }

        [HttpGet]
        public ActionResult GetTemplateSave()
        {
            return PartialView("~/Views/CoSSIP/SaveTemplate.cshtml");
        }

        [HttpGet]
        public ActionResult GetTemplateLoad()
        {
            Template[] templates = db.GetUserTemplates(userName());

            string[] templateNames = new string[templates.Length];
            string[] markups = new string[templates.Length];
            string[] tables = new string[templates.Length];
            string[] fields = new string[templates.Length];
            string[] filters = new string[templates.Length];

            for (int i = 0; i < templates.Length; i++)
            {
                Template template = templates[i];

                templateNames[i] = template.templateName;
                markups[i] = template.templateMarkup;
                tables[i] = template.templateTables;
                fields[i] = template.templateFields;
                filters[i] = template.templateFilters;
            }

            return PartialView("~/Views/CoSSIP/LoadTemplate.cshtml", new AllTemplateModels{ user = db.GetUser(userName()), templateNames = templateNames, markups = markups, tables = tables, fields = fields, filters = filters });
        }


        [HttpPost]
        public ActionResult GetTemplate(string templateName)
        {
            Template template = db.GetTemplate(userName(), templateName);

            string[] result = { template.templateMarkup, template.templateTables, template.templateFields, template.templateFilters, template.templateLayoutMarkup };

            return Json(result);
        }

        [HttpGet]
        public ActionResult PostTemplate(string tables, string fields, string filters)
        {
            string tempTables = JsonConvert.DeserializeObject<string>(tables);
            string tempFields = JsonConvert.DeserializeObject<string>(fields);
            string tempFilters = JsonConvert.DeserializeObject<string>(filters);
            
            JavaScriptSerializer jss = new JavaScriptSerializer();

            string[] chosenTables = jss.Deserialize<string[]>(tempTables);
            string[][] chosenFields = jss.Deserialize<string[][]>(tempFields);
            string[][] chosenFilters = jss.Deserialize<string[][]>(tempFilters);


            string[][] data = db.GetData(chosenTables, chosenFields, chosenFilters);

            return PartialView("~/Views/CoSSIP/ViewTemplate.cshtml", new ViewTemplatePageModel { user = db.GetUser(userName()), ResultData = data });
        }
        #endregion
    }
}
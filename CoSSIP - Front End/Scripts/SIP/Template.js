﻿/*
 * File Name - Template.js
 *
 * Author - Tristyn Mackay
 * Date Created - 09/10/2017
 * Last Edited - 26/10/2017
 * Last Edited by - Tristyn Mackay
 *
 * Description -
 */


$(document).ready(
    function () {
        window.onbeforeunload = function (e) {
            if (JSON.parse(document.getElementById("chosenTables").value).length > 0) {
                e = e || window.event;

                // For IE and Firefox prior to version 4
                if (e) {
                    e.returnValue = 'Are you sure you want to leave the current page?';
                }

                // For Safari
                return 'Are you sure you want to leave the current page?';
            }
        };
    }
);


$(window).unload(
    function () {
        document.getElementById("chosenTables").value = "[]";
        document.getElementById("chosenFields").value = "[]";
        document.getElementById("chosenTypes").value = "[]";
    }
);


/* 
 * 
 */
window.onclick = function (event) {
    if (!event.target.className.match(/inputButton/)) {
        closeCurrentInput();
    }
}


function closeCurrentInput() {
    var dropdowns = document.getElementsByClassName("dropdown-content");
    var i;
    for (i = 0; i < dropdowns.length; i++) {
        var openDropdown = dropdowns[i];
        if (openDropdown.classList.contains('show')) {
            openDropdown.classList.remove('show');
        }
    }
}


/* 
 * Create a new Visible Object component.
 */
function addSearchTable() {
    var container = document.getElementById("SearchTablesContainer");
    var content = document.createElement('div');

    var visibleTables = document.getElementsByClassName("SearchTableContent");
    // Check if there are visible tables.
    if (visibleTables != null) {
        var tables = JSON.parse(document.getElementById("dataTables").value);
        // Check if the user has already selected the maximum amount of tables.
        if (visibleTables.length >= tables.length) {
            alert("You have already created the MAXIMUM amount of Table Objects!");
            return;
        }
        // Check if there is another empty table that still needs filling.
        for (var i = 0; i < visibleTables.length; i++) {
            var currID = visibleTables[i].getElementsByClassName("dropdown")[0].id;

            if (currID == null || currID.length < 1) {
                alert("You still need to FILL in an Empty Object.");
                return;
            }
        }
    }

    content.style.backgroundColor = "#98878F";

    content.className = "container SearchTableContent col-lg-11 col-md-11 col-sm-11 col-xs-11";
    content.style.border = "solid 1px black";
    content.style.padding = "15px 0px 15px 15px";
    content.style.margin = "0px 0px 20px 0px";
    content.style.borderRadius = "5px 5px 5px 5px";
    content.style.boxShadow = "1px 1px 2px 3px";

    content.innerHTML =
        "    <button class=\"stateButton container col-lg-1 col-md-1 col-sm-1 col-xs-1\" onclick=\"removeObject(this);\" style=\"background-color: indianred;\">\n" +
        "    	<span class=\"shine\"></span>\n" +
        "       <span class=\"text\" style=\"color: black;\">-</span>\n" +
        "    </button>\n" +
        "    <div class=\"dropdown col-lg-2 col-md-2 col-sm-2 col-xs-2\">\n" +
        "        <button class=\"inputButton container\" onclick=\"getObjects(this);\" style=\"background-color: lightcoral; width: 100%; height: 100%;\">\n" +
        "            <span class=\"shine\"></span>\n" +
        "            <span class=\"text\" style=\"color: black;\">Object</span>\n" +
        "            <span class=\"caret\"></span>\n" +
        "        </button>\n" +
        "        <div class=\"dropdown-content\">\n" +
        "        </div>\n" +
        "    </div>\n" +
        "    <div class=\"container col-lg-1 col-md-1 col-sm-1 col-xs-1\">\n" +
        "        <span class=\"glyphicon glyphicon-remove\" style=\"margin:12px 12px 12px 12px; color: red;\"></span>\n" +
        "    </div>\n" +
        "    <div class=\"container SearchFieldsContainer col-lg-8 col-md-8 col-sm-8 col-xs-8\">\n" +
        "        <div class=\"container SearchFieldContent col-lg-12 col-md-12 col-sm-12 col-xs-12\">\n" +
        "        </div>\n" +
        "        <div class=\"col-lg-3 col-md-3 col-sm-3 col-xs-3\">\n" +
        "            <button class=\"stateButton col-lg-6 col-md-6 col-sm-6 col-xs-6\" style=\"margin: 5px 0px 0px 35px; background-color: #53b455;\" onclick=\"addSearchField(this); return false;\">" +
        "    	         <span class=\"shine\"></span>\n" +
        "                <span class=\"text\" style=\"color: black;\">+</span>\n" +
        "            </button>\n" +
        "        </div>\n" +
        "        <div class=\"col-lg-9 col-md-9 col-sm-9 col-xs-9\">\n" +
        "        </div>\n" +
        "    </div>\n";

    container.appendChild(content);
}

/* 
 * Get the Objects that can be selected.
 */
function getObjects(callingObject) {
    var tables = JSON.parse(document.getElementById("dataTables").value);
    var currentTables = JSON.parse(document.getElementById("chosenTables").value);

    var dropDown = closest(callingObject, "div");
    var dropDownContents = dropDown.getElementsByClassName("dropdown-content")[0];

    dropDownContents.innerHTML = "";

    // Iterate over each of the tables to help find the unselected tables.
    for (var i = 0; i < tables.length; i++) {
        var canAdd = true;
        // Iterate over the current tables to find the unselected tables.
        for (var k = 0; k < currentTables.length; k++) {
            // If the current selected table equals the current unselected table,
            // then this table already exists and can't be added.
            if (currentTables[k] == tables[i]) {
                canAdd = false;
                break;
            }
        }
        if (canAdd) {
            var result = tables[i];
            dropDownContents.innerHTML += "                        <a href=\"#\" onClick=\"selectObject(this, '" + result + "'); return false;\">" + result + "</li>\n";
        }
    }
    dropDownContents.classList.toggle("show");
}

/* 
 * Select and set the Object.
 */
function selectObject(callingObject, objectName) {
    var tables = JSON.parse(document.getElementById("dataTables").value);
    var currentTables = JSON.parse(document.getElementById("chosenTables").value);

    var dropDownContent = closest(callingObject, "div");
    var dropDown = closest(dropDownContent, "div.dropdown");
    var button = dropDown.getElementsByClassName("inputButton")[0];
    var text = button.getElementsByClassName("text")[0];

    // Check if the id of the object is already set.
    if (dropDown.id != null && dropDown.id.length > 0) {
        // Clear the table from the 'chosen' list.
        removeTable( "" + dropDown.id);

        // Find all the table's field objects and remove them.
        var searchFields = closest(dropDown, "div.SearchTableContent").getElementsByClassName("SearchFieldContent");
        for (var i = 0; i < searchFields.length; i++) {
            searchFields[i].innerHTML = "";
        }
    }

    // Loop through the currently selected tables and check if this table already exists.
    var canAdd = true;
    for (var i = 0; i < currentTables.length; i++) {
        if (currentTables[i] == objectName) {
            canAdd = false;
            break;
        }
    }

    // Add the table to the selected list.
    if (canAdd) {
        button.style.backgroundColor = "#80dc82";
        // Set the table id and text.
        dropDown.id = "" + objectName;
        text.innerHTML = "" + objectName;
        // Change the colour and image of the related cross/tick.
        var glyphIcons = closest(dropDown, "div.SearchTableContent").getElementsByClassName("glyphicon")
        var glyphIcon = glyphIcons[glyphIcons.length - 1];
        glyphIcon.className = "glyphicon glyphicon-ok";
        glyphIcon.style.color = "#00ca34";
        // Add the table to the current tables.
        currentTables.push(objectName);
        document.getElementById("chosenTables").value = JSON.stringify(currentTables);
    }
}

/* 
 * Remove an Object from the set of Visible Objects.
 */
function removeObject(callingObject) {
    var container = closest(callingObject, "div.SearchTableContent");
    var selectedTable = container.getElementsByClassName("dropdown")[0].id;

    if (selectedTable != null && selectedTable.length > 0) {
        removeTable("" + selectedTable);
    }
    $(container).remove();
}

/* 
 * Remove a table from the Current Tables list.
 */
function removeTable(tableToRemove) {
    var tables = JSON.parse(document.getElementById("chosenTables").value);
    for (var i = 0; i < tables.length; i++) {
        if (tables[i] == tableToRemove) {

            // Remove the fields from the table.
            var tableFields = JSON.parse(document.getElementById("chosenFields").value);
            var tableFilters = JSON.parse(document.getElementById("chosenTypes").value);
            if (tableFields.length > 0) {
                // Remove the the fields from the table.
                // This will also remove all related field filters.
                removeFieldTable(tableFields, i);
                // Turn the result into a string and clear the related arrays [].
                var temp = JSON.stringify(tableFields).split(',[]').join('');
                temp = temp.split('[],').join('');
                temp = temp.split('[]').join('');
                // Now set the global hidden value for the current fields to the result.
                document.getElementById("chosenFields").value = temp;

                var temp2 = JSON.stringify(tableFilters[i]).split('\"\",').join('');
                temp2 = temp2.split(',\"\"').join('');
                temp2 = temp2.split('\"\"').join('');
                tableFilters[i] = JSON.parse(temp2);
                var temp3 = JSON.stringify(tableFilters).split(',[]').join('');
                temp3 = temp3.split('[],').join('');
                temp3 = temp3.split('[]').join('');
                // Now set the global hidden value for the current fields to the result.
                document.getElementById("chosenTypes").value = temp3;
            }

            // Remove the table.
            tables = removeFromArray(tables, tableToRemove);
            document.getElementById("chosenTables").value = JSON.stringify(tables);
        }
    }
}



/* 
 * Create a new Visible Object Field Component.
 */
function addSearchField(callingObject) {
    var parentTable = closest(callingObject, "div.SearchFieldsContainer")
    parentTable = closest(parentTable, "div.SearchTableContent").getElementsByClassName("dropdown")[0].id;

    if (parentTable == null || parentTable.length < 1) {
        alert("Please select a correct OBJECT to Select a Field");
        return;
    }

    var container = closest(callingObject, "div.SearchFieldsContainer").getElementsByClassName("SearchFieldContent")[0];
    var content = document.createElement('div');

    var tables = JSON.parse(document.getElementById("dataTables").value);

    var visibleFields = container.getElementsByClassName("SearchFieldObject");


    // Check if there are visible Fields.
    if (visibleFields != null) {
        var fields = JSON.parse(document.getElementById("dataFields").value);
        // Iterate through the tables to find the correct table.
        for (var k = 0; k < tables.length; k++) {
            // Check if the current table is the same as the field's table.
            if (tables[k] == parentTable) {
                // Check if the user has already selected the maximum amount of fields.
                if (visibleFields.length >= fields[k].length) {
                    alert("You have already created the MAXIMUM amount of Fields for this Object!");
                    return;
                }
                // Check if there is another empty table that still needs filling.
                for (var i = 0; i < visibleFields.length; i++) {
                    var currID = visibleFields[i].getElementsByClassName("dropdown")[0].id;

                    if (currID == null || currID.length < 1) {
                        alert("You still need to FILL in an Empty Field.");
                        return;
                    }
                }
            }
        }
    }

    var currentTables = JSON.parse(document.getElementById("chosenTables").value);
    var currentFields = JSON.parse(document.getElementById("chosenFields").value);
    var currentFilters = JSON.parse(document.getElementById("chosenTypes").value);
    // Check if the fields array has been populated yet.
    if (currentFields.length < currentTables.length) {
        var diff = currentTables.length - currentFields.length;
        for (var i = 0; i < diff; i++) {
            currentFields.push([]);
            currentFilters.push([]);
        }
        document.getElementById("chosenFields").value = JSON.stringify(currentFields);
        document.getElementById("chosenTypes").value = JSON.stringify(currentFilters);
    }


    content.style.backgroundColor = "#494E6B";

    content.className = "container SearchFieldObject col-lg-12 col-md-12 col-sm-12 col-xs-12";
    content.style.border = "solid 1px black";
    content.style.padding = "10px 10px 10px 10px";
    content.style.margin = "0px 0px 15px 20px";
    content.style.borderRadius = "5px 5px 5px 5px";
    content.style.boxShadow = "1px 1px 2px 3px";

    content.innerHTML =
        "    <div class=\"container col-lg-6 col-md-6 col-sm-6 col-xs-6\">\n" +
        "        <button class=\"stateButton col-lg-3 col-md-3 col-sm-3 col-xs-3\" onclick=\"removeFieldObject(this); return false;\" style=\"background-color: indianred;\">\n" +
        "    	     <span class=\"shine\"></span>\n" +
        "            <span class=\"text\" style=\"color: black;\">-</span>\n" +
        "        </button>\n" +
        "        <div class=\"dropdown col-lg-6 col-md-6 col-sm-6 col-xs-6\">\n" +
        "            <button class=\"inputButton container\" onclick=\"getFields(this); return false;\" style=\"background-color: lightcoral; width: 100%; height: 100%;\">\n" +
        "                <span class=\"shine\"></span>\n" +
        "                <span class=\"text\" style=\"color: black;\">Field</span>\n" +
        "                <span class=\"caret\"></span>\n" +
        "            </button>\n" +
        "            <div class=\"dropdown-content\">\n" +
        "            </div>\n" +
        "        </div>\n" +
        "        <div class=\"col-lg-3 col-md-3 col-sm-3 col-xs-3\">\n" +
        "            <span class=\"glyphicon glyphicon-remove\" style=\"margin:12px 12px 12px 12px; color: red;\"></span>\n" +
        "        </div >\n" +
        "    </div>\n" +
        "    <div class=\"container SearchFiltersContainer col-lg-6 col-md-6 col-sm-6 col-xs-6\">\n" +
        "        <div class=\"container SearchFilterContent col-lg-12 col-md-12 col-sm-12 col-xs-12\">\n" +
        "        </div>\n" +
        "        <button class=\"stateButton col-lg-3 col-md-3 col-sm-3 col-xs-3\" style=\"margin: 5px 0px 0px 15px; background-color: #53b455;\" onclick=\"addSearchComponentFilter(this); return false;\">" +
        "    	     <span class=\"shine\"></span>\n" +
        "            <span class=\"text\" style=\"color: black;\">+</span>\n" +
        "        </button >\n" +
        "        <div class=\"col-lg-9 col-md-9 col-sm-9 col-xs-9\">\n" +
        "        </div >\n" +
        "    </div>\n";
    
    container.appendChild(content);
}

/* 
 * Get the Object Fields that can be selected.
 */
function getFields(callingObject) {
    var tables = JSON.parse(document.getElementById("dataTables").value);
    var currentTables = JSON.parse(document.getElementById("chosenTables").value);

    var tableFields = JSON.parse(document.getElementById("dataFields").value);
    var currentFields = JSON.parse(document.getElementById("chosenFields").value);

    var dropDown = closest(callingObject, "div.dropdown");
    var dropDownContents = dropDown.getElementsByClassName("dropdown-content")[0];
    var parentTable = closest(dropDown, "div.SearchTableContent").getElementsByClassName("dropdown")[0].id;

    dropDownContents.innerHTML = "";

    // Iterate through all tables until you find the parent table.
    for (var i = 0; i < tables.length; i++) {
        // Check if the current table is the same as the field's table.
        if (tables[i] == parentTable) {
            // Now iterate over the currently 'visible' tables to find the table related to the field.
            for (var k = 0; k < currentTables.length; k++) {
                // Check if the current table is the same as the field's table.
                if (currentTables[k] == tables[i]) {
                    // Now iterate over the data table's fields.
                    for (var j = 0; j < tableFields[i].length; j++) {
                        var canAdd = true;
                        // Check each of these fields if they are currently selected.
                        for (var p = 0; p < currentFields[k].length; p++) {
                            if (tableFields[i][j] == currentFields[k][p]) {
                                canAdd = false;
                                break;
                            }
                        }
                        // Add the fields that aren't currently selected.
                        if (canAdd) {
                            currentFields[k].push(tableFields[i][j]);
                            var result = currentFields[k][currentFields[k].length - 1];
                            dropDownContents.innerHTML += "                        <a href=\"#\" onClick=\"selectField(this, '" + result + "'); return false;\">" + result + "</li>\n";
                        }
                    }
                    break;
                }
            }
            break;
        }
    }

    dropDownContents.classList.toggle("show");
}

/* 
 * Select and set the Object Field.
 */
function selectField(callingObject, fieldName) {
    var tables = JSON.parse(document.getElementById("dataTables").value);
    var currentTables = JSON.parse(document.getElementById("chosenTables").value);

    var tableFields = JSON.parse(document.getElementById("dataFields").value);
    var currentFields = JSON.parse(document.getElementById("chosenFields").value);

    var tableFilters = JSON.parse(document.getElementById("dataFields").value);
    var currentFilters = JSON.parse(document.getElementById("chosenTypes").value);


    var dropDown = closest(callingObject, "div.dropdown");
    var dropDownContent = dropDown.getElementsByClassName('dropdown-content');
    var selectedField = dropDown.id;
    var parentTable = closest(dropDown, "div.SearchTableContent").getElementsByClassName("dropdown")[0].id;
    var button = dropDown.getElementsByClassName("inputButton")[0];
    var text = button.getElementsByClassName("text")[0];

    var parentTableIndex = 0;


    // Check if the id of the object is already set.
    if (selectedField != null && selectedField.length > 0) {
        // Iterate through the 'chosen' tables to find the one related to this field.
        for (var i = 0; i < currentTables.length; i++) {
            // Check if current table is the same as the field's table.
            if (currentTables[i] == parentTable) {
                // Now iterate through the table's fields to find the correct field.
                for (var k = 0; k < currentFields[i].length; k++) {
                    if (currentFields[i][k] == selectedField) {
                        // Clear the field from the 'chosen' list.
                        removeField(currentFields, i, "" + selectedField);
                        // Clear the filter from the 'chosen' list.
                        removeFilter(i, k);
                        break;
                    }
                }
                break;
            }
        }
    }

    // Loop through the currently selected fields and check if this field already exists.
    var canAdd = true;
    for (var i = 0; i < currentTables.length; i++) {
        // Check if the current table is the same as the field's table.
        if (currentTables[i] == parentTable) {
            parentTableIndex = i;
            // Now iterate through the table's fields to find the correct field.
            for (var k = 0; k < currentFields[i].length; k++) {
                // Check if the current field is the same as the given field.
                if (currentFields[i][k] == fieldName) {
                    canAdd = false;
                    break;
                }
            }
            break;
        }
    }
    // Add the Field to the selected list.
    if (canAdd) {
        button.style.backgroundColor = "#80dc82";

        // Set the id and field text.
        dropDown.id = "" + fieldName;
        text.innerHTML = "" + fieldName;

        // Change the colour and image of the related cross/tick.
        var glyphIcons = closest(dropDown, "div.SearchFieldObject").getElementsByClassName("glyphicon")
        var glyphIcon = glyphIcons[glyphIcons.length - 1];
        glyphIcon.className = "glyphicon glyphicon-ok";
        glyphIcon.style.color = "#00ca34";

        // Add the field to the current fields table.
        currentFields[parentTableIndex].push(fieldName);
        document.getElementById("chosenFields").value = JSON.stringify(currentFields);
        // Add an empty filter to the current field.
        currentFilters[parentTableIndex].push("");
        document.getElementById("chosenTypes").value = JSON.stringify(currentFilters);


        // Add the field to the layout table.
        // Check if the current fields contains 2 fields after inserting 
        if (currentFields[parentTableIndex].length > 1) {
            // There is a predecessor within this table, so add the field behind the last element in the table.
            addLayoutField(currentFields[parentTableIndex][currentFields[parentTableIndex].length - 2], currentTables[parentTableIndex], fieldName, parentTable);
        }
        else if (currentFields[parentTableIndex - 1] != null && currentFields[parentTableIndex - 1].length > 0) {
            // There is a predecessor within the previous table, so add the field behind the last element in the previous table.
            addLayoutField(currentFields[parentTableIndex - 1][currentFields[parentTableIndex - 1].length - 1], currentTables[parentTableIndex - 1], fieldName, parentTable);
        }
        else {
            // There is no predecessor so pass the method with a null for predecessor.
            addLayoutField(null, null, fieldName, parentTable);
        }
    }
}

/* 
 * Remove an Object Field from the set of Visible Object Fields.
 */
function removeFieldObject(callingObject) {
    var container = closest(callingObject, "div.SearchFieldObject");
    var dropDown = container.getElementsByClassName("dropdown")[0];
    var selectedField = dropDown.id;
    
    var parentTable = closest(dropDown, "div.SearchTableContent").getElementsByClassName("dropdown")[0].id;

    var currentTables = JSON.parse(document.getElementById("chosenTables").value);
    
    var currentFields = JSON.parse(document.getElementById("chosenFields").value);

    var currentFilters = JSON.parse(document.getElementById("chosenTypes").value);
    

    // Check if the id of the object is already set.
    if (selectedField != null && selectedField.length > 0) {
        // Iterate through the 'chosen' tables to find the one related to this field.
        for (var i = 0; i < currentTables.length; i++) {
            // Check if current table is the same as the field's table.
            if (currentTables[i] == parentTable) {
                // Now iterate through the table's fields to find the correct field.
                for (var k = 0; k < currentFields[i].length; k++) {
                    if (currentFields[i][k] == selectedField) {
                        // Clear the field from the 'chosen' list.
                        removeField(currentFields, i, selectedField);
                        break;
                    }
                }
                break;
            }
        }
    }
    $(container).remove();
}

/* 
 * Remove an Object Field from the Current Fields List.
 */
function removeField(tableFields, tableIDToRemoveFrom, fieldToRemove) {
    var tables = JSON.parse(document.getElementById("chosenTables").value);

    // Remove the field from the layout table.
    removeLayoutField(fieldToRemove, tables[tableIDToRemoveFrom]);

    var fieldIDToRemoveFrom = tableFields[tableIDToRemoveFrom].indexOf(fieldToRemove);

    // Remove the filter from the current field.
    removeFilter(tableIDToRemoveFrom, fieldIDToRemoveFrom);

    // Remove the field from the current fields.
    var temp = tableFields[tableIDToRemoveFrom];
    temp.splice(fieldIDToRemoveFrom, 1);
    tableFields[tableIDToRemoveFrom] = temp;

    document.getElementById("chosenFields").value = JSON.stringify(tableFields);
}

/* 
 * Remove an Object Field from the Current Fields List.
 */
function removeFieldTable(tableFields, tableIDToRemove) {
    for (var i = tableFields[tableIDToRemove].length - 1; i >= 0; i--) {
        var tables = JSON.parse(document.getElementById("chosenTables").value);
                
        // Remove the field from the current fields.
        removeField(tableFields, tableIDToRemove, tableFields[tableIDToRemove][i]);
    }
}



/* 
 * Add a Field Filter Component.
 */
function addSearchComponentFilter(element) {
    var container = closest(element, "div.SearchFiltersContainer").getElementsByClassName("SearchFilterContent")[0];
    var parentField = closest(container, "div.SearchFieldObject");
    var fieldID = parentField.getElementsByClassName("dropdown")[0].id;

    // Check if the parent field has been selected yet.
    if (fieldID == null || fieldID.length <= 0) {
        alert("You must select a field before attempting to create a filter for it.");
        return;
    }

    var content = document.createElement('div');

    content.style.backgroundColor = "#192231";

    content.className = "container SearchFilterObject col-lg-12 col-md-12 col-sm-12 col-xs-12";
    content.style.border = "solid 1px black";
    content.style.padding = "10px 10px 10px 10px";
    content.style.margin = "0px 0px 10px 0px";
    content.style.borderRadius = "5px 5px 5px 5px";
    content.style.boxShadow = "1px 1px 2px 3px";

    content.innerHTML =
        "    <button class=\"stateButton col-lg-3 col-md-3 col-sm-3 col-xs-3\" onclick=\"removeFilterObject(this); return false;\" style=\"background-color: indianred;\">\n" +
        "    	<span class=\"shine\"></span>\n" +
        "       <span class=\"text\" style=\"color: black;\">-</span>\n" +
        "    </button>\n" +
        "    <div class=\"filter col-lg-6 col-md-6 col-sm-6 col-xs-6\" onclick=\"return false;\">\n" +
        "    </div>\n";

    container.appendChild(content);

    createFilter(content);

    $(element).remove();
}


/*
 * Create the filter type/HTML.
 */
function createFilter(container) {
    var parentField = closest(container, "div.SearchFieldObject");
    var parentFieldID = parentField.getElementsByClassName("dropdown")[0].id;
    var parentTable = closest(parentField, "div.SearchTableContent");
    var parentTableID = parentTable.getElementsByClassName("dropdown")[0].id;

    var currentTables = JSON.parse(document.getElementById("chosenTables").value);
    var currentFields = JSON.parse(document.getElementById("chosenFields").value);
    var currentFilters = JSON.parse(document.getElementById("chosenTypes").value);
    

    // Iterate through the current tables to find the related table.
    for (var i = 0; i < currentTables.length; i++) {
        if (currentTables[i] == parentTableID) {
            // Iterate through the current table's fields to find the related field.
            for (var k = 0; k < currentFields[i].length; k++) {
                if (currentFields[i][k] == parentFieldID) {
                    // Add the filter.
                    container.getElementsByClassName("filter")[0].innerHTML = generateFilterHtml(getFilterType(parentTableID, parentFieldID));
                    break;
                }
            }
            break;
        }
    }
    return "";
}
/*
 * Helper method to get the filter type.
 */
function getFilterType(tableName, fieldName) {
    var tables = JSON.parse(document.getElementById("dataTables").value);
    var fields = JSON.parse(document.getElementById("dataFields").value);
    var filters = JSON.parse(document.getElementById("dataTypes").value);

    for (var i = 0; i < tables.length; i++) {
        if (tables[i] == tableName) {
            for (var k = 0; k < fields[i].length; k++) {
                if (fields[i][k] == fieldName) {
                    return filters[i][k];
                }
            }
            break;
        }
    }
    return "";
}
/*
 * Helper method to return the filter type HTML.
 */
function generateFilterHtml(type) {
    switch (type) {
        case "int":
            return "    <input style=\"width: 100%; height: 40px;\" type=\"number\" step=\"1\" value=\"1\" onchange=\"updateFilter(this);\" />\n";
        case "float":
            return "    <input style=\"width: 100%; height: 40px;\" type=\"number\" step=\"0.00001\" value=\"1\" onchange=\"updateFilter(this);\" />\n";
        case "datetime":
            return "    <input style=\"width: 100%; height: 40px;\" type=\"datetime-local\" value=\"1996-12-19\" onchange=\"updateFilter(this);\" />\n";
        case "string":
            return "    <input style=\"width: 100%; height: 40px;\" type=\"text\" min=\"1\" value=\"\" onchange=\"updateFilter(this);\" />\n";
        default:
            break;
    }
}

/*
 * Update the current filter data when a field has been changed.
 */
function updateFilter(callingObject) {
    var container = closest(callingObject, "div.SearchFilterObject");

    var parentField = closest(container, "div.SearchFieldObject");
    var parentFieldID = parentField.getElementsByClassName("dropdown")[0].id;

    var parentTable = closest(parentField, "div.SearchTableContent");
    var parentTableID = parentTable.getElementsByClassName("dropdown")[0].id;

    var currentTables = JSON.parse(document.getElementById("chosenTables").value);
    var currentFields = JSON.parse(document.getElementById("chosenFields").value);
    var currentFilters = JSON.parse(document.getElementById("chosenTypes").value);

    // Check through the current tables for the one related to this filter's parent table.
    for (var i = 0; i < currentTables.length; i++) {
        if (currentTables[i] == parentTableID) {
            // Check through the current fields for the one related to this filter's parent field.
            for (var k = 0; k < currentFields[i].length; k++) {
                if (currentFields[i][k] == parentFieldID) {
                    currentFilters[i][k] = "" + callingObject.value;
                    document.getElementById("chosenTypes").value = JSON.stringify(currentFilters);
                    break;
                }
            }
            break;
        }
    }
}

/* 
 * Remove a Filter Field from the Current Filters List.
 */
function removeFilter(tableIDToRemoveFrom, fieldIDToRemove) {
    var tableFilters = JSON.parse(document.getElementById("chosenTypes").value);

    // Remove the filter from the current filters, fieldIDToRemove = filterID due to string[][] array.
    var temp = tableFilters[tableIDToRemoveFrom];
    temp.splice(fieldIDToRemove, 1);
    tableFilters[tableIDToRemoveFrom] = temp;

    // Convert the chosen filters back to their hidden html element.
    document.getElementById("chosenTypes").value = JSON.stringify(tableFilters);
}

/*
 * Remove a Filter Component from the page.
 */
function removeFilterObject(callingObject) {
    var container = closest(callingObject, "div.SearchFilterObject");

    var parentField = closest(container, "div.SearchFieldObject");
    var parentFieldID = parentField.getElementsByClassName("dropdown")[0].id;
    
    var parentTable = closest(parentField, "div.SearchTableContent");
    var parentTableID = parentTable.getElementsByClassName("dropdown")[0].id;

    var currentTables = JSON.parse(document.getElementById("chosenTables").value);
    var currentFields = JSON.parse(document.getElementById("chosenFields").value);
    var currentFilters = JSON.parse(document.getElementById("chosenTypes").value);
    
    // Check through the current tables for the one related to this filter's parent table.
    for (var i = 0; i < currentTables.length; i++) {
        if (currentTables[i] == parentTableID) {
            // Check through the current fields for the one related to this filter's parent field.
            for (var k = 0; k < currentFields[i].length; k++) {
                if (currentFields[i][k] == parentFieldID) {
                    // Remove the filter from the filter's array.
                    currentFilters[i][k] = "";
                    document.getElementById("chosenTypes").value = JSON.stringify(currentFilters);

                    var filterContainer = closest(container, "div.SearchFilterContent");
                    // Re-Insert the add filter button for this field.
                    var temp = document.createElement('button');
                    temp.className = "stateButton col-lg-3 col-md-3 col-sm-3 col-xs-3";
                    temp.style.margin = "5px 0px 0px 15px";
                    temp.style.backgroundColor = "#53b455";
                    temp.onclick = function () { addSearchComponentFilter(this); };
                    temp.innerHTML =    "    	     <span class=\"shine\"></span>\n" +
                                        "            <span class=\"text\">+</span>\n";
                    insertAfter(temp, filterContainer);
                    break;
                }
            }
            break;
        }
    }
    $(container).remove();
}




/*
 * Use this to add a table field to the template layout table.
 */
function addLayoutField(predecessorField, predecessorTable, fieldName, tableName) {
    var resultTable = document.getElementById("ResultTableHead");
    var headers = resultTable.getElementsByTagName("th");

    // Check if there are any headers already in the table.
    if (headers.length > 0) {
        // Check each header if it is the field that comes before the one we are trying to add.
        for (var i = 0; i < headers.length; i++) {
            // Get the header ID.
            var headerID = headers[i].id;
            // Split the ID so that the first element is the table name, 
            // and the second element is the field name.
            var headerDetails = headerID.split("-");
            // Check if the header's table/field names match the predecessor's table/field names.
            if (headerDetails[0] == predecessorTable && headerDetails[1] == predecessorField) {
                // Create a new header element to be added.
                var header = document.createElement('th');
                // Set the ID to "tableName-fieldName" for later reference.
                header.id = tableName + "-" + fieldName;
                // Set the innerHTML to display the field name.
                header.innerHTML = fieldName;
                // Now insert the new header after the predecessor header.
                if (headers.length - 1 == i) {
                    resultTable.appendChild(header);
                    break;
                }
                else {
                    insertAfter(headers[i], header);
                    break;
                }
            }
        }
    }
    else {
        // Add the header to the table, as there are none to check against.
        resultTable.innerHTML = "<th id=\"" + tableName + "-" + fieldName + "\">" + fieldName + "</th>\n";
    }
}

/*
 * Use this to replace the layout table field with a new one.
 */
function replaceLayoutField(replaceFieldName, tableName, addFieldName) {
    var resultTable = document.getElementById("ResultTableHead");
    var headers = resultTable.getElementsByClassName("th");

    // Check if there are any headers.
    if (headers.length > 0) {
        for (var i = 0; i < headers; i++) {
            // Get the header's ID's.
            var headerID = headers[i].id;
            var headerDetails = headerTable.split("-");
            // Check if the ID's are equal to the ones we need to replace.
            if (headerDetails[0] == tableName && headerDetails[1] == replaceFieldName) {
                // Change the field name to the new name.
                headers[i].innerHTML = addFieldName;
                // Change the header ID.
                headers[i].id = tableName + "-" + addFieldName;
            }
        }
    }
}

/*
 * Use this to remove a layout table field. 
 */
function removeLayoutField(fieldName, tableName) {
    var resultTable = document.getElementById("ResultTableHead");
    var headers = resultTable.getElementsByTagName("th");

    // Check if there is anything that can be removed.
    if (headers.length > 0) {
        // Check each of the headers if they are the header to be removed.
        for (var i = 0; i < headers.length; i++) {
            // Get the currently selected header ID.
            var headerID = headers[i].id;
            // Split the ID into 2 strings, table ID, and fieldID.
            var headerDetails = headerID.split("-");
            // Check the tableID and fieldID if they are the same as the ones to replace.
            if (headerDetails[0] == tableName && headerDetails[1] == fieldName) {
                // Remove the header.
                $(headers[i]).remove();
            }
        }
    }
}


/*
 * Get the Save Panel Partial View.
 */
function GetSavePanel() {
    $.ajax({
        type: "GET",
        url: "/CoSSIP/GetTemplateSave",
        dataType: "html",
        traditional: true,
        success: function (result) {
            $('#saveContent').html(result);
        }
    });
}

/*
 * Get the Load Panel Partial View.
 */
function GetLoadPanel() {
    $.ajax({
        type: "GET",
        url: "/CoSSIP/GetTemplateLoad",
        dataType: "html",
        traditional: true,
        success: function (result) {
            $('#loadContent').html(result);
        }
    });
}


/*
 * Save the given template name and template.
 */
function FinishSave() {
    var templateName = document.getElementById("templateSaveName").value;
    var markup = document.getElementById("SearchTablesContainer").innerHTML;
    var tables = document.getElementById("chosenTables").value;
    var fields = document.getElementById("chosenFields").value;
    var filters = document.getElementById("chosenTypes").value;
    var layoutMarkup = document.getElementById("resultLayoutTable").innerHTML;

    if (confirm("Are you sure you want to save this template as\"" + templateName + "\"?")) {
        var template = { 'templateName': templateName, 'markup': markup, 'tables': tables, 'fields': fields, 'filters': filters, 'layoutMarkup': layoutMarkup };

        $.ajax({
            type: "POST",
            url: "/CoSSIP/PostTemplateSave",
            data: JSON.stringify(template),
            dataType: "json",
            contentType: "application/json",
            success: function (result) {

            }
        });
    }
}

/*
 * Load the selected template in the load panel.
 */
function FinishLoad(callingObject) {
    var templateName = callingObject.id;

    if (confirm("Are you sure you want to load the template \"" + templateName + "\"?")) {
        $.ajax({
            type: "POST",
            url: "/CoSSIP/GetTemplate",
            data: JSON.stringify({ 'templateName': templateName }),
            dataType: "json",
            contentType: "application/json",
            traditional: true,
            success: function (result) {
                // Implement the markup.
                $('#SearchTablesContainer').html(result[0]);
                // Implement the chosen tables.
                document.getElementById("chosenTables").value = result[1];
                // Implement the chosen fields.
                document.getElementById("chosenFields").value = result[2];
                // Implement the chosen filters.
                document.getElementById("chosenTypes").value = result[3];
                // Implement the layout markup.
                $('#resultLayoutTable').html(result[4]);
            }
        });
    }
}

/*
 * Search the database with the template chosen tables/fields/filters.
 */
function PostTemplate() {
    var tables = JSON.stringify(document.getElementById("chosenTables").value);
    var fields = JSON.stringify(document.getElementById("chosenFields").value);
    var filters = JSON.stringify(document.getElementById("chosenTypes").value);

    $.ajax({
        type: "GET",
        url: "/CoSSIP/PostTemplate",
        data: { 'tables':tables, 'fields':fields, 'filters':filters },
        dataType: "html",
        traditional: true,
        success: function (result) {
            $('#DynamicContent').html(result);
        }
    });
}



/*
 * Get the results of the last search performed,
 * If no search has been performed you will get an empty 2 dimensional JSON array back.
 */
function GetResults() {
    result = JSON.parse(document.getElementById("results").value);
    return result;
}


function ExportResults() {
    var results = GetResults();

    var csv = "";

    for (i = 0; i < results[0].length; i++) {
        csv += results[0][i];
        if (i != results.length - 1) {
            csv +=(", ");
        }
    }
    csv += "\n";

    for (i = 0; i < results[1].length; i++) {

        csv += results[1][i];
        if (i != results.length - 1) {
            csv += ", ";
        }
    }

    var filename = prompt("Please enter the file name you wish to save as...");

    var blobObject = new Blob([csv]);

    window.navigator.msSaveBlob(blobObject, filename + ".csv");

}
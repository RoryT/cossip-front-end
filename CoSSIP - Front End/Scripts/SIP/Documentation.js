﻿/*
 * File Name - Documenation.js
 *
 * Author - Tristyn Mackay
 * Date Created - 08/10/2017
 * Last Edited - 13/10/2017
 * Last Edited by - Tristyn Mackay
 *
 * Description - 
 */


/*
 * Create the starting method for the script here.
 */
$(document).ready(
    function () {
        displaySysDocs();
        displayProjDocs();
    }
);


/*
 * Method to display the buttons linked to System Documentation.
 */
function displaySysDocs() {
    var systemDocumentation = document.getElementById("SystemDocumentation");
    var div = document.createElement('div');

    div.className = 'jumbotron container';
    div.innerHTML =
        "    <!-- Header. -->\n" +
        "    <div>\n" +
        "        <h2>System Documentation</h2>\n" +
        "    </div>\n" +
        "    <!-- Descriptive Text. -->\n" +
        "    <div>\n" +
        "        <p class=\"lead\">\n" +
        "            Information regarding System Documentation can be found here.\n" +
        "        </p>\n" +
        "    </div>\n" +
        "    <!-- Buttons. -->\n" +
        "    <div>\n" +
        "        <button class=\"inputButton col-lg-3 col-md-3 col-sm-3 col-xs-3\" onclick=\"return false;\">\n" +
        "            <span class=\"shine\"></span>\n" +
        "            <span class=\"text\">Back-End System</span>\n" +
        "        </button>\n" +
        "        <div class=\"col-lg-1 col-md-1 col-sm-1 col-xs-1\"></div>\n" +
        "        <button class=\"inputButton col-lg-3 col-md-3 col-sm-3 col-xs-3\" onclick=\"return false;\">\n" +
        "            <span class=\"shine\"></span>\n" +
        "            <span class=\"text\">Front-End System</span>\n" +
        "        </button>\n" +
        "        <div class=\"col-lg-1 col-md-1 col-sm-1 col-xs-1\"></div>\n" +
        "        <button class=\"inputButton col-lg-3 col-md-3 col-sm-3 col-xs-3\" onclick=\"return false;\">\n" +
        "            <span class=\"shine\"></span>\n" +
        "            <span class=\"text\">Front-End Screens</span>\n" +
        "        </button>\n" +
        "        <div class=\"row\" style=\"padding: 30px 30px 30px 30px;\"></div>\n" +
        "        <button class=\"inputButton col-lg-3 col-md-3 col-sm-3 col-xs-3\" onclick=\"return false;\">\n" +
        "            <span class=\"shine\"></span>\n" +
        "            <span class=\"text\">Back-End Structure Flow</span>\n" +
        "        </button>\n" +
        "        <div class=\"col-lg-1 col-md-1 col-sm-1 col-xs-1\"></div>\n" +
        "        <button class=\"inputButton col-lg-3 col-md-3 col-sm-3 col-xs-3\" onclick=\"return false;\">\n" +
        "            <span class=\"shine\"></span>\n" +
        "            <span class=\"text\">Front-End Structure Flow</span>\n" +
        "        </button>\n" +
        "        <div class=\"col-lg-1 col-md-1 col-sm-1 col-xs-1\"></div>\n" +
        "        <button class=\"inputButton col-lg-3 col-md-3 col-sm-3 col-xs-3\" onclick=\"return false;\">\n" +
        "            <span class=\"shine\"></span>\n" +
        "            <span class=\"text\">CoSSIP Database</span>\n" +
        "        </button>\n" +
        "    </div>\n";

    systemDocumentation.appendChild(div);
}

/*
 * Method to display the buttons linked to Project Documentation.
 */
function displayProjDocs() {
    var systemDocumentation = document.getElementById("ProjectDocumentation");
    var div = document.createElement('div');

    div.className = 'jumbotron container';
    div.innerHTML =
        "    <!--Header. -->\n" +
        "    <div>\n" +
        "        <h2>Project Documentation</h2>\n" +
        "    </div>\n" +
        "    <!--Descriptive Text. -->\n" +
        "    <div>\n" +
        "        <p class=\"lead\">\n" +
        "            Information regarding Project Documentation can be found here.\n" +
        "        </p>\n" +
        "    </div>\n" +
        "    <!--Buttons. -->\n" +
        "    <div>\n" +
        "        <button class=\"inputButton col-lg-3 col-md-3 col-sm-3 col-xs-3\" onclick=\"return false;\">\n" +
        "            <span class=\"shine\"></span>\n" +
        "            <span class=\"text\">Business Case</span>\n" +
        "        </button>\n" +
        "        <div class=\"col-lg-1 col-md-1 col-sm-1 col-xs-1\"></div>\n" +
        "        <button class=\"inputButton col-lg-3 col-md-3 col-sm-3 col-xs-3\" onclick=\"return false;\">\n" +
        "            <span class=\"shine\"></span>\n" +
        "            <span class=\"text\">Project Plan</span>\n" +
        "        </button>\n" +
        "        <div class=\"col-lg-1 col-md-1 col-sm-1 col-xs-1\"></div>\n" +
        "        <button class=\"inputButton col-lg-3 col-md-3 col-sm-3 col-xs-3\" onclick=\"return false;\">\n" +
        "            <span class=\"shine\"></span>\n" +
        "            <span class=\"text\">Stakeholders</span>\n" +
        "        </button>\n" +
        "        <div class=\"row\" style=\"padding: 30px 30px 30px 30px;\"></div>\n" +
        "        <button class=\"inputButton col-lg-3 col-md-3 col-sm-3 col-xs-3\" onclick=\"return false;\">\n" +
        "            <span class=\"shine\"></span>\n" +
        "            <span class=\"text\">Requirements</span>\n" +
        "        </button>\n" +
        "        <div class=\"col-lg-1 col-md-1 col-sm-1 col-xs-1\"></div>\n" +
        "        <button class=\"inputButton col-lg-3 col-md-3 col-sm-3 col-xs-3\" onclick=\"return false;\">\n" +
        "            <span class=\"shine\"></span>\n" +
        "            <span class=\"text\">Risk Register</span>\n" +
        "        </button>\n" +
        "        <div class=\"col-lg-1 col-md-1 col-sm-1 col-xs-1\"></div>\n" +
        "        <button class=\"inputButton col-lg-3 col-md-3 col-sm-3 col-xs-3\" onclick=\"return false;\">\n" +
        "            <span class=\"shine\"></span>\n" +
        "            <span class=\"text\">Use Cases</span>\n" +
        "        </button>\n" +
        "    </div>\n";

    systemDocumentation.appendChild(div);
}
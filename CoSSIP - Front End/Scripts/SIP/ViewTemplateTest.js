﻿$(document).ready(function () {
    var enqIDs = ["1",
                    "2",
                    "3",
                    "4",
                    "5"];

    var enqNames = ["Fell Tree",
                    "Road Obstruction",
                    "Curb Broken",
                    "Pot-Hole",
                    "Vandalised Asset"];

    var dateReceiveds = ["2012-09-23",
                            "2015-10-16",
                            "2016-04-09",
                            "2017-06-03",
                            "2017-09-27"];

    var jobIDs = ["1",
                    "2",
                    "3",
                    "4",
                    "5",
                    "6"]

    var jobNames = ["Fix Asset",
                    "Check Road",
                    "Fix Curb",
                    "Fix Pot-Hole",
                    "Clean up Litter"];

    var jobStatuses = ["JSO1",
                        "JSO2",
                        "JSO3",
                        "JSO4",
                        "JSO5"];

    var actionOfficerIDs = ["AO1",
                            "AO2",
                            "AO3",
                            "AO4",
                            "AO5"];


    var tableContainer = document.getElementById("ResultTable");
    var tableHeadersContainer = document.getElementById("ResultTableHeaders");
    var tableResultsContainer = document.getElementById("ResultTableContent")

    tableHeadersContainer.innerHTML = "<th>enquiryID</th>";
    tableHeadersContainer.innerHTML += "<th>enquiryName</th>";
    tableHeadersContainer.innerHTML += "<th>dateReceived</th>";
    tableHeadersContainer.innerHTML += "<th>enquiryStatus</th>";
    tableHeadersContainer.innerHTML += "<th>actionOfficerID</th>";
    tableHeadersContainer.innerHTML += "<th>jobID</th>";
    tableHeadersContainer.innerHTML += "<th>jobName</th>";

    for (var i = 0; i < enqIDs.length; i++) {
        tableResultsContainer.innerHTML +=
            "<tr>" +
            "<td>" + enqIDs[i] + "</td>" +
            "<td>" + enqNames[i] + "</td>" +
            "<td>" + dateReceiveds[i] + "</td>" +
            "<td>" + jobStatuses[i] + "</td>" +
            "<td>" + actionOfficerIDs[i] + "</td>" +
            "<td>" + jobIDs[i] + "</td>" +
            "<td>" + jobNames[i] + "</td>" +
            "</tr>";
    }
});


﻿/*
 * File Name - SIP_Tables.cs
 *
 * Author - Tristyn Mackay
 * Date Created - 24/10/2017
 * Last Edited - 26/10/2017
 * Last Edited by - Tristyn Mackay
 *
 * Description -
 */

namespace CoSSIP___Front_End.Models
{
    public class SIP_Tables
    {
        public enum TableNames{
            ConfCustReq,
            PathCustReq,
            PathNotes,
            ConfJobs,

            NONE
        }

        // These are important for front-end GUI use.
        // Modify these if the tables differ from your database.
        #region - Tables as strings array.
        private string[] tables = { "C_Customer_Request",
                                    "PW_Customer_Request",
                                    "PW_Notes",
                                    "C_Jobs"};
        #endregion
        
        #region - Fields as string arrays.
        #region - Data table fields.
        private static string[] ConfCustReq_Fields = {  "CRequestNumber",       // This is the foreign key connecting the confirm ID for this enquiry.
                                                        "PWRequestNumber",      // This is the foreign key connecting the pathways ID for this enquiry.
                                                        "requestDescription",
                                                        "serviceCode",
                                                        "serviceType",
                                                        "siteCode",             
                                                        "requesteeName",
                                                        "requestAddress",
                                                        "actionOfficer",
                                                        "actionOfficerCode",    // This is the foreign key connecting an action officer to this enquiry.   
                                                        "requesteeHomephone",
                                                        "requesteeMobilephone",
                                                        "requesteeFax",
                                                        "requesteeEmail",
                                                        "plotNumber",
                                                        "lastJob",
                                                        "lastJobNotes",};

        private static string[] PathCustReq_Fields = {  "PWRequestNumber",      // This is the foreign key connecting pathways enquiry to this request.
                                                        "requestStatus",    
                                                        "requestType",
                                                        "requestCode",
                                                        "priority",
                                                        "dateRecieved",
                                                        "dateRespondBy",
                                                        "dateResponded",
                                                        "timeTaken",
                                                        "timeTakenUnit",
                                                        "PWRecievingOfficer",
                                                        "PWActioningOfficer",
                                                        "requesteeType",
                                                        "contactMethod",
                                                        "PWReqKey"};

        private static string[] Jobs_Fields = { "jobNumber",
                                                "siteCode",
                                                "plotNumber",
                                                "jobLogNumber",
                                                "jobEnteredDate",
                                                "jobCompletedDate",
                                                "jobNotes",
                                                "jobLocation",
                                                "customerName",
                                                "costCode",
                                                "contractCode",
                                                "contractAreaCode",};
        private static string[] Notes_Fields = {"PWNoteKey",
                                                "PWReqKey",
                                                "author",
                                                "date",
                                                "noteType",
                                                "note"};
        #endregion
        #endregion
        
        #region - Filters as string arrays.
        #region - Data related table filters.
        private static string[] ConfCustReq_Filters = { "int",
                                                        "int",
                                                        "string",
                                                        "string",
                                                        "string",
                                                        "string",
                                                        "string",
                                                        "string",
                                                        "string",
                                                        "string",
                                                        "string",
                                                        "string",
                                                        "string",
                                                        "string",
                                                        "float",
                                                        "float",
                                                        "string"};
        private static string[] PathCustReq_Filters = { "int",
                                                        "string",
                                                        "string",
                                                        "string",
                                                        "int",
                                                        "datetime",
                                                        "datetime",
                                                        "datetime",
                                                        "int",
                                                        "string",
                                                        "string",
                                                        "string",
                                                        "string",
                                                        "string",
                                                        "int"};
        private static string[] Jobs_Filters = {"float",
                                                "string",
                                                "float",
                                                "float",
                                                "datetime",
                                                "datetime",
                                                "string",
                                                "string",
                                                "string",
                                                "string",
                                                "string",
                                                "string"};
        private static string[] Notes_Filters = {   "int",
                                                    "int",
                                                    "string",
                                                    "datetime",
                                                    "string",
                                                    "string"};
        #endregion
        #endregion
        //-----!


        #region - Table Key related Methods.
        // Get Primary Keys
        private string GetTablePKeyByTableName(TableNames table)
        {
            switch (table)
            {
                case TableNames.ConfCustReq:
                    return "CRequestNumber";

                case TableNames.PathCustReq:
                    return "PWRequestNumber";

                case TableNames.ConfJobs:
                    return "jobNumber";

                case TableNames.PathNotes:
                    return "PWNoteKey";

                default:
                    return null;
            }
        }
        // String helper version of get PK.
        public string GetTablePKeyByTableName(string tableName)
        {
            return GetTablePKeyByTableName(GetTableAsEnum(tableName)); ;
        }

        // Get Foreign Keys between tables.
        private string GetTableFKeyByTableName(TableNames fromTable, TableNames toTable)
        {
            switch (fromTable)
            {
                case TableNames.ConfCustReq:
                    switch (toTable)
                    {
                        case TableNames.PathCustReq:
                            return "PWRequestNumber";

                        default:
                            return null;
                    }


                case TableNames.PathCustReq:
                    switch (toTable)
                    {
                        case TableNames.ConfCustReq:
                            return "PWRequestNumber";

                        case TableNames.PathNotes:
                            return "PWReqKey";

                        default:
                            return null;
                    }


                case TableNames.PathNotes:
                    switch (toTable)
                    {
                        case TableNames.PathCustReq:
                            return "PWRequestNumber";

                        default:
                            return null;
                    }


                case TableNames.ConfJobs:
                    switch (toTable)
                    {
                        case TableNames.ConfCustReq:
                            return "jobNumber";

                        default:
                            return null;
                    }

                default:
                    return null;
            }
        }
        // String helper version of get FK.
        public string GetTableFKeyByTableName(string fromTableName, string toTableName)
        {
            return GetTableFKeyByTableName(GetTableAsEnum(fromTableName), GetTableAsEnum(toTableName));
        }
        #endregion

        #region - Table related Methods.
        // Get the string array representation of all the table names.
        public string[] GetAllTables()
        {
            return tables;
        }

        // Get the table as an enum from the table name.
        private TableNames GetTableAsEnum(string tableName)
        {
            switch (tableName)
            {
                case "C_Customer_Request":
                    return TableNames.ConfCustReq;

                case "PW_Customer_Request":
                    return TableNames.PathCustReq;

                case "PW_Notes":
                    return TableNames.ConfJobs;

                case "C_Jobs":
                    return TableNames.PathNotes;

                default:
                    return TableNames.NONE;
            }
        }
        #endregion


        #region - Field related Methods.
        // Get the string array representation of All Fields of a table.
        public string[] GetAllFieldsByTableName(TableNames table)
        {
            switch (table)
            {

                case TableNames.ConfCustReq:
                    return ConfCustReq_Fields;

                case TableNames.PathCustReq:
                    return PathCustReq_Fields;

                case TableNames.ConfJobs:
                    return Notes_Fields;

                case TableNames.PathNotes:
                    return Jobs_Fields;

                default:
                    return null;
            }
        }
        // String helper version of get all fields.
        public string[] GetAllFieldsByTableName(string tableName)
        {
            return GetAllFieldsByTableName(GetTableAsEnum(tableName));
        }

        // Get All Fields.
        public string[][] GetAllFields()
        {
            string[] allTables = GetAllTables();
            string[][] allFields = new string[allTables.Length][];

            for (int i = 0; i < allTables.Length; i++)
            {
                allFields[i] = GetAllFieldsByTableName(allTables[i]);
            }

            return allFields;
        }
        #endregion


        #region - Filter related Methods.
        // Get the string array representation of All Filters of a table.
        public string[] GetAllFiltersByTableName(TableNames table)
        {
            switch (table)
            {
                case TableNames.ConfCustReq:
                    return ConfCustReq_Filters;

                case TableNames.PathCustReq:
                    return PathCustReq_Filters;

                case TableNames.ConfJobs:
                    return Jobs_Filters;

                case TableNames.PathNotes:
                    return Notes_Filters;

                default:
                    return null;
            }
        }
        // String helper version of get all filters.
        public string[] GetAllFiltersByTableName(string tableName)
        {
            return GetAllFiltersByTableName(GetTableAsEnum(tableName));
        }

        // Get All Filters.
        public string[][] GetAllFilters()
        {

            string[] allTables = GetAllTables();
            string[][] result = new string[allTables.Length][]; ;



            for (int i = 0; i < allTables.Length; i++)
            {
                result[i] = GetAllFiltersByTableName(allTables[i]);
            }

            return result;
        }
        #endregion
    }
}
﻿using System.Collections.Generic;

namespace CoSSIP___Front_End.Models
{
    public class AllTemplateModels : ViewModelBase
    {
        public string[] templateNames { get; set; }
        public string[] markups { get; set; }
        public string[] tables { get; set; }
        public string[] fields { get; set; }
        public string[] filters { get; set; }
        public string[] layoutMarkups { get; set; }
    }

    public class TemplateModel : ViewModelBase
    {
        public string templateName { get; set; }
        public string markup { get; set; }
        public string tables { get; set; }
        public string fields { get; set; }
        public string filters { get; set; }
        public string layoutMarkup { get; set; }
    }
}
﻿/*
 * File Name - SIP_Repository.cs
 *
 * Author - Tristyn Mackay
 * Date Created - 24/10/2017
 * Last Edited - 26/10/2017
 * Last Edited by - Tristyn Mackay
 *
 * Description -
 */

using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Linq.Dynamic;

namespace CoSSIP___Front_End.Models
{
    public class SIP_Repository
    {
        private SqlConnection connection;
        private SqlCommand command;

        //private LocalDBEntities db = new LocalDBEntities();
        private SIPDB_Entities db = new SIPDB_Entities();
        private SIP_Tables sipTables = new SIP_Tables();

        bool debug = false;


        /// <summary>
        /// The Default Constructor.
        /// </summary>
        /// <param name="dbConnectionString"></param>
        public SIP_Repository(string dbConnectionString)
        {
            if (debug)
            {
                connection = new SqlConnection("Data Source=(LocalDB)\\MSSQLLocalDB; AttachDbFilename = \"D:\\Tristyn's Projects\\SIP-FrontEnd\\CoSSIP - Front End\\App_Data\\LocalDB.mdf\"; Integrated Security = True");
            }
            else
            {
                connection = new SqlConnection(dbConnectionString);
            }

            command = new SqlCommand
            {
                Connection = connection,
                CommandType = System.Data.CommandType.Text
            };
        }


        #region - User Get Methods.
        // Get a user's ID by their username.
        public int GetUserID(string username)
        {

            return (from x in db.Users
                    where x.userName == username
                    select x.userID).SingleOrDefault();
        }

        // Get a users roles from the database.
        private string[] GetUserRoles(int userID)
        {
            // Get all the user's roles.
            return (from x in db.Roles
                    join userRole in db.UserRoles on x.roleID equals userRole.roleID
                    join user in db.Users on userRole.userID equals user.userID
                    where user.userID == userID
                    select x.roleName).ToArray();
        }
        // Helper method for get user roles.
        public string[] GetUserRoles(string username)
        {
            return GetUserRoles(GetUserID(username));
        }

        // Get a user's details by their username.
        public UserModel GetUser(string username)
        {
            // Get the user's ID.
            int userId = GetUserID(username);

            // Create an object to hold information about the User.
            return new UserModel
            {
                userName = username,
                userID = userId,
                roles = GetUserRoles(userId)
            };
        }
        #endregion


        #region - Template Get Methods.
        // Get a template, given the username and the template name.
        public Template GetTemplate(string userName, string templateName)
        {
            int userID = GetUserID(userName);

            return (from x in db.Templates
                    where x.userID == userID && x.templateName == templateName
                    select x).SingleOrDefault();
        }

        // Get a user's saved Templates.
        public Template[] GetUserTemplates(string userName)
        {
            int userID = GetUserID(userName);

            return (from x in db.Templates
                    where x.userID == userID
                    select x).ToArray();
        }

        // Get all save Templates.
        public Template[] GetAllTemplates()
        {
            return (from x in db.Templates
                    select x).ToArray();
        }
        #endregion


        #region - Data Get Methods.
        public string[][] GetData(string[] tablesToUse, string[][] fieldsToGet, string[][] restrictions)
        {
            string froms = "FROM ";
            string joins = "";
            string wheres = "WHERE ";
            string selects = "SELECT ";

            int whereCount = 0;


            #region - Build the initial part of the query.
            // For every Table.
            for (int i = 0; i < tablesToUse.Length; i++)
            {
                if (i == 0)
                {
                    // Setup the table this is all coming(starting) from.
                    froms += "[" + tablesToUse[i] + "] ";
                }
                else
                {
                    // Get the Foreign Key that relates these tables.
                    string foreignKey = sipTables.GetTableFKeyByTableName(tablesToUse[i], tablesToUse[i - 1]);
                    // Add the new JOIN to the joins string.
                    joins += "JOIN [" + tablesToUse[i] + "] ON [" + tablesToUse[i - 1] + "].[" + foreignKey + "]=[" + tablesToUse[i] + "].[" + foreignKey + "] ";
                }

                // Check if there are any fields for this table.
                if (fieldsToGet[i] != null && fieldsToGet[i].Length > 0)
                {
                    // For every Field in this table.
                    for (int k = 0; k < fieldsToGet[i].Length; k++)
                    {
                        // Add the field in the search portion of the query.
                        selects += "[" + tablesToUse[i] + "].[" + fieldsToGet[i][k] + "], ";

                        if (restrictions[i][k].Length > 0)
                        {
                            // Add the new WHERE parameter to the wheres string.
                            wheres += "[" + tablesToUse[i] + "].[" + fieldsToGet[i][k] + "] = @where" + whereCount + " AND ";
                            // Add one to the where count for later parameter filling.
                            whereCount++;
                        }
                    }
                }
            }
            #endregion


            #region - Remove trailing values from initial query Build.
            // Remove the trailing ',' from the SELECT section.
            if (selects != null && selects.Length > 7)
            {
                if (selects[selects.Length - 2] == ',')
                {
                    selects = selects.Remove(selects.Length - 2, 1);
                }
            }
            
            // Remove the trailing 'AND' from the WHERE section.
            if (wheres != null && wheres.Length > 6)
            {
                if (wheres.Substring(wheres.Length - 4, 3).Equals("AND"))
                {
                    // Remove the trailing AND as well as the trailing space.
                    wheres = wheres.Remove(wheres.Length - 4, 4);
                }
            }
            #endregion


            #region - Build the command string.
            command.CommandText = selects + froms;
            if (joins.Length > 0)
            {
                command.CommandText += joins;
            }
            if (whereCount > 0)
            {
                command.CommandText += wheres;
            }
            #endregion


            #region - Setup and organize the filters.
            // Create a list for organizing the filters.
            List<string> filters = new List<string>();
            // Organize the filters into the new list by first iterating through the tables.
            for (int i = 0; i < restrictions.Length; i++)
            {
                // If the current Table has any fields.
                if (restrictions[i] != null)
                {
                    // Iterate through the Fields.
                    for (int k = 0; k < restrictions[i].Length; k++)
                    {
                        // If the current Field has any Filters.
                        if (restrictions[i][k] != null && restrictions[i][k].Length > 0)
                        {
                            // Add the Filter to the sorted filters list.
                            filters.Add(restrictions[i][k]);
                        }
                    }
                }
            }

            // Convert the sorted filters list to an array.
            string[] sortedFilters = filters.ToArray();

            // Now insert the filter values into the command string.
            for (int i = 0; i < whereCount; i++)
            {
                string filter = sortedFilters[i];
                int intValue = 0;
                float floatValue = 0;
                DateTime dateTimeValue = new DateTime();

                // Test if INT, then FLOAT, then DATETIME, otherwise its a STRING.
                if (int.TryParse(filter, out intValue))
                {
                    command.Parameters.AddWithValue("@where" + i, intValue);
                }
                else if (float.TryParse(filter, out floatValue))
                {
                    command.Parameters.AddWithValue("@where" + i, floatValue);
                }
                else if (DateTime.TryParse(filter, out dateTimeValue))
                {
                    command.Parameters.AddWithValue("@where" + i, dateTimeValue);
                }
                else
                {
                    command.Parameters.AddWithValue("@where" + i, filter);
                }
            }
            #endregion

            command.CommandText += ";";

            #region - Connect to the DB and gather the data.
            List<List<string>> data = new List<List<string>>();
            // Now Open the connection and attempt to get the data back.
            try
            {
                connection.Open();

                using (SqlDataReader dbReader = command.ExecuteReader())
                {
                    // Check if the reader has any rows.
                    if (dbReader.HasRows)
                    {
                        data.Add(new List<string>());

                        // Grab the headers for the columns and store them in the first row in the data.
                        for (int i = 0; i < dbReader.FieldCount; i++)
                        {
                            data[0].Add(dbReader.GetName(i));
                        }

                        int rowIndex = 1;
                        // Read through the rows.
                        while (dbReader.Read())
                        {
                            data.Add(new List<string>());

                            // Grab the current data row.
                            for (int i = 0; i < dbReader.FieldCount; i++)
                            {
                                // Add the column data to the row in the data set.
                                data[rowIndex].Add(dbReader.GetValue(i).ToString());
                            }

                            rowIndex++;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("Failed to open database...");
                Console.WriteLine(connection.ConnectionString);
                Console.WriteLine(ex.Message);
            }
            finally
            {
                connection.Close();
            }
            #endregion

            // Return the data converted to an array.
            string[][] result = new string[data.Count()][];
            for (int i = 0; i < data.Count(); i++)
            {
                result[i] = new string[data[i].Count()];
                for (int k = 0; k < data[i].Count(); k++)
                {
                    result[i][k] = data[i][k];
                }
            }

            return result;
        }
        #endregion


        #region - Table Get Methods.
        public string[] GetAllTables()
        {
            return sipTables.GetAllTables();
        }
        #endregion
        
        #region - Field Get Methods.
        public string[] GetAllFieldsByTableName(string tableName)
        {
            return sipTables.GetAllFieldsByTableName(tableName);
        }

        public string[][] GetAllFields()
        {
            return sipTables.GetAllFields();
        }
        #endregion

        #region - Filter Get Methods.
        public string[] GetAllFiltersByTableName(string tableName)
        {
            return sipTables.GetAllFiltersByTableName(tableName);
        }

        public string[][] GetAllFilters()
        {
            return sipTables.GetAllFilters();
        }
        #endregion


        #region - Create Methods.
        #region - Create Roles.
        // Add a Role to the database.
        public Boolean addRole(string role, string description)
        {
            string query = "INSERT INTO dbo.Roles " +
                            "VALUES (@rolename, @roledescription);";
            command.CommandText = query;
            command.Parameters.AddWithValue("@rolename", role);
            command.Parameters.AddWithValue("@roledescription", description);

            return RunInsertQuery(query);
        }
        // Add Role helper method.
        public Boolean addRole(string role)
        {
            return addRole(role, "");
        }
        #endregion

        #region - Create Users.
        // Add a User to the database.
        public Boolean addUser(string userLogin, string userFirstName, string userLastName, string userRole)
        {
            string query = "INSERT INTO dbo.Users " +
                            "VALUES (@loginname, @firstname, @lastname);";
            command.CommandText = query;
            command.Parameters.AddWithValue("@loginname", userLogin);
            command.Parameters.AddWithValue("@firstname", userFirstName);
            command.Parameters.AddWithValue("@lastname", userLastName);

            // Create the user in the database.
            if (RunInsertQuery(query) == false)
            {
                // Log that the user wasn't inserted.
                return false;
            }
            // Link the user to an application Role.
            if (addUserToRole(userLogin, userRole) == false)
            {
                // Log that the user wasn't assigned to a role.
                return false;
            }

            return true;
        }
        // Add User helper method.
        public Boolean addUser(string userLogin, string userFirstName, string userLastName)
        {
            return addUser(userLogin, userFirstName, userLastName, "Guest");
        }
        #endregion

        #region - Add User to a Role.
        // Add a User to a Role in the database.
        public Boolean addUserToRole(string userLogin, string roleName)
        {
            int userID = (from x in db.Users
                         where x.userName == userLogin
                         select x.userID).SingleOrDefault();

            int roleID = (from x in db.Roles
                         where x.roleName == roleName
                         select x.roleID).SingleOrDefault();

            string query = "INSERT INTO dbo.UserRoles " +
                            "VALUES (" + userID + ", " + roleID + ");";
            command.CommandText = query;

            return RunInsertQuery(query);
        }
        #endregion

        #region - Create Template.
        public Boolean addTemplate(string userName, string templateName, string markup, string tables, string fields, string filters, string templateLayoutMarkup)
        {
            int userID = GetUserID(userName);

            string query = "INSERT INTO dbo.Template " +
                            "VALUES (@userID, @templateName, @templateMarkup, @templateTables, @templateFields, @templateFilters, @templateLayoutMarkup);";
            command.CommandText = query;
            command.Parameters.AddWithValue("@userID", userID);
            command.Parameters.AddWithValue("@templateName", templateName);
            command.Parameters.AddWithValue("@templateMarkup", markup);
            command.Parameters.AddWithValue("@templateTables", tables);
            command.Parameters.AddWithValue("@templateFields", fields);
            command.Parameters.AddWithValue("@templateFilters", filters);
            command.Parameters.AddWithValue("@templateLayoutMarkup", templateLayoutMarkup);

            // Create the user in the database.
            if (RunInsertQuery(query) == false)
            {
                // Log that the user wasn't inserted.
                return false;
            }

            return true;
        }
        #endregion
        #endregion


        // Use this to run insert queries.
        public Boolean RunInsertQuery(string _command)
        {
            command.CommandText = _command;

            // Log that the command is starting.
            System.Diagnostics.Debug.WriteLine("Running SQL Command: ");
            System.Diagnostics.Debug.WriteLine(command.CommandText);

            // Log the start time.
            DateTime started = DateTime.Now;
            System.Diagnostics.Debug.WriteLine("Started At " + started);

            try
            {
                connection.Open();

                command.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                // Log a failure to connect to the database.
                Console.WriteLine("Failed to open database...");
                Console.WriteLine(connection.ConnectionString);
                Console.WriteLine(ex.Message);

                return false;
            }
            finally
            {
                // Close the connection to the DB.
                connection.Close();
                // Log the finish time.
                DateTime finished = DateTime.Now;
                System.Diagnostics.Debug.WriteLine("Finished At " + finished);
                // Log the duration of the insert operation.
                System.Diagnostics.Debug.WriteLine("Took " + finished.Subtract(started));
            }

            return true;
        }
    }
}
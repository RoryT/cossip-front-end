﻿using System.Collections.Generic;

namespace CoSSIP___Front_End.Models
{
    public class TemplatePageModel : ViewModelBase
    {
        public string[] Tables { get; set; }
        public string[][] Fields { get; set; }
        public string[][] FieldTypes { get; set; }
    }
}
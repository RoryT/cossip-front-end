﻿using System.Collections.Generic;

namespace CoSSIP___Front_End.Models
{
    public class ViewTemplatePageModel : ViewModelBase
    {
        public string[][] ResultData { get; set; }
    }
}
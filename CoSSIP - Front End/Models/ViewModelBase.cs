﻿/*
 * File Name - ViewModelBase.cs
 *
 * Author - Rory Thoman
 * Date Created - 06/10/2017
 * Last Edited - 25/10/2017
 * Last Edited by - Tristyn Mackay
 *
 * Description -
 */
namespace CoSSIP___Front_End.Models
{
    public class ViewModelBase
    {
        #region - Variables.
        public UserModel user { get; set; }
        #endregion
    }
}
﻿/*
 * File Name - UsersModel.cs
 *
 * Author - Tristyn Mackay
 * Date Created - 24/10/2017
 * Last Edited - 26/10/2017
 * Last Edited by - Tristyn Mackay
 *
 * Description -
 */
using System.Collections.Generic;

namespace CoSSIP___Front_End.Models
{
    public class UserModel
    {
        public string userName { get; set; }
        public int userID { get; set; }
        public string[] roles { get; set; }
    }
}